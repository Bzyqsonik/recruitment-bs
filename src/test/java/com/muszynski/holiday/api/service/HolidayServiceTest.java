package com.muszynski.holiday.api.service;

import com.muszynski.holiday.api.client.model.Holiday;
import com.muszynski.holiday.api.client.model.HolidayExternalApiRequest;
import com.muszynski.holiday.api.client.service.HolidayExternalApiService;
import com.muszynski.holiday.api.model.HolidayRequest;
import com.muszynski.holiday.api.model.HolidayResponse;
import lombok.Value;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.Set;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class HolidayServiceTest {

    private static final Fixtures fixtures = new Fixtures();
    private static final int MONTH_INTERVAL = 3;

    @Mock
    private HolidayExternalApiService holidayExternalApiService;
    private HolidayService holidayService;

    @Before
    public void setUp() {
        holidayService = new HolidayService(holidayExternalApiService, MONTH_INTERVAL);
    }

    @Test
    public void shouldFindCommonHolidays() {
        //given
        given(holidayExternalApiService.findHolidays(fixtures.primaryCountryClientRequestForFirstQuarter))
                .willReturn(fixtures.firstQuarterPrimaryCountryHolidays);
        given(holidayExternalApiService.findHolidays(fixtures.secondaryCountryClientRequestForFirstQuarter))
                .willReturn(fixtures.firstQuarterSecondaryCountryHolidays);
        given(holidayExternalApiService.findHolidays(fixtures.primaryCountryClientRequestForSecondQuarter))
                .willReturn(fixtures.secondQuarterPrimaryCountryHolidays);
        given(holidayExternalApiService.findHolidays(fixtures.secondaryCountryClientRequestForSecondQuarter))
                .willReturn(fixtures.secondQuarterSecondaryCountryHolidays);

        //when
        final HolidayResponse response = holidayService.findHolidaysAtTheSameDayInBothCountries(fixtures.holidayRequest);

        //then
        assertThat(response).isEqualTo(fixtures.holidayResponse);
    }

    @Value
    private static class Fixtures {
        Set<Holiday> firstQuarterPrimaryCountryHolidays = Stream.of(
                Holiday.builder()
                        .name("Holiday1PL")
                        .date("2016-12-25")
                        .build(),
                Holiday.builder()
                        .name("Holiday2PL")
                        .date("2017-02-01")
                        .build(),
                Holiday.builder()
                        .name("Holiday3PL")
                        .date("2017-03-05")
                        .build()
        ).collect(toSet());
        Set<Holiday> firstQuarterSecondaryCountryHolidays = Stream.of(
                Holiday.builder()
                        .name("Holiday1DE")
                        .date("2016-12-26")
                        .build(),
                Holiday.builder()
                        .name("Holiday2DE")
                        .date("2017-02-02")
                        .build(),
                Holiday.builder()
                        .name("Holiday3DE")
                        .date("2017-03-06")
                        .build()
        ).collect(toSet());
        Set<Holiday> secondQuarterPrimaryCountryHolidays = Stream.of(
                Holiday.builder()
                        .name("Holiday4PL")
                        .date("2017-03-29")
                        .build(),
                Holiday.builder()
                        .name("Holiday5PL")
                        .date("2017-04-07")
                        .build(),
                Holiday.builder()
                        .name("Holiday6PL")
                        .date("2017-06-06")
                        .build()
        ).collect(toSet());
        Set<Holiday> secondQuarterSecondaryCountryHolidays = Stream.of(
                Holiday.builder()
                        .name("Holiday4DE")
                        .date("2017-03-07")
                        .build(),
                Holiday.builder()
                        .name("Holiday5DE")
                        .date("2017-04-07")
                        .build(),
                Holiday.builder()
                        .name("Holiday6DE")
                        .date("2017-06-06")
                        .build()
        ).collect(toSet());

        LocalDate baseDate = LocalDate.of(2017, 1, 1);
        String primaryCountry = "PL";
        String secondaryCountry = "DE";

        HolidayExternalApiRequest primaryCountryClientRequestForFirstQuarter = HolidayExternalApiRequest.builder()
                .date(LocalDate.of(2017, 4, 1))
                .country(primaryCountry)
                .build();
        HolidayExternalApiRequest secondaryCountryClientRequestForFirstQuarter = HolidayExternalApiRequest.builder()
                .date(LocalDate.of(2017, 4, 1))
                .country(secondaryCountry)
                .build();
        HolidayExternalApiRequest primaryCountryClientRequestForSecondQuarter = HolidayExternalApiRequest.builder()
                .date(LocalDate.of(2017, 7, 1))
                .country(primaryCountry)
                .build();
        HolidayExternalApiRequest secondaryCountryClientRequestForSecondQuarter = HolidayExternalApiRequest.builder()
                .date(LocalDate.of(2017, 7, 1))
                .country(secondaryCountry)
                .build();

        HolidayRequest holidayRequest = HolidayRequest.builder()
                .date(baseDate)
                .primaryCountry(primaryCountry)
                .secondaryCountry(secondaryCountry)
                .build();

        HolidayResponse holidayResponse = HolidayResponse.builder()
                .date(LocalDate.of(2017, 4, 7))
                .primaryCountryHolidayName("Holiday5PL")
                .secondaryCountryHolidayName("Holiday5DE")
                .build();
    }
}
