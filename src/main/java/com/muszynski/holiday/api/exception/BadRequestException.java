package com.muszynski.holiday.api.exception;

import lombok.NonNull;

public class BadRequestException extends RuntimeException {

    public BadRequestException(@NonNull final String message) {
        super(message);
    }
}
