package com.muszynski.holiday.api.exception;

public class CommonHolidaysNotFoundException extends RuntimeException {
    public CommonHolidaysNotFoundException(final String message) {
        super(message);
    }
}
