package com.muszynski.holiday.api.exception;

public class InconsistentDataException extends RuntimeException {
    public InconsistentDataException(final String message) {
        super(message);
    }

    public InconsistentDataException() {
        super();
    }
}
