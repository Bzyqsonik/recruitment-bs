package com.muszynski.holiday.api.controller;

import lombok.Value;

@Value(staticConstructor = "of")
class Error {
    String message;
}
