package com.muszynski.holiday.api.controller;

import com.muszynski.holiday.api.exception.BadRequestException;
import com.muszynski.holiday.api.model.HolidayRequest;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Set;

import static java.time.format.DateTimeFormatter.ISO_DATE;
import static reactor.core.publisher.Mono.error;
import static reactor.core.publisher.Mono.just;

@Component
class RequestMapper {

    private static final String PRIMARY_COUNTRY_ATTRIBUTE = "primaryCountry";
    private static final String SECONDARY_COUNTRY_ATTRIBUTE = "secondaryCountry";
    private static final String DATE_ATTRIBUTE = "date";
    private final Set<String> supportedCountries;


    RequestMapper(@Value("#{'${holiday.api.countries}'.split(',')}") final Set<String> supportedCountries) {
        this.supportedCountries = supportedCountries;
    }

    Mono<HolidayRequest> toHolidayRequest(@NonNull final ServerRequest serverRequest) {
        try {
            return just(createHolidayRequest(serverRequest));
        } catch (final Throwable e) {
            return error(e);
        }
    }

    private HolidayRequest createHolidayRequest(final ServerRequest serverRequest) throws BadRequestException {
        final String primaryCountry = getPrimaryCountry(serverRequest);
        final String secondaryCountry = getSecondaryCountry(serverRequest);
        if (primaryCountry.equals(secondaryCountry)) {
            throw new BadRequestException("Countries should be different!");
        }
        return HolidayRequest.builder()
                .primaryCountry(primaryCountry)
                .secondaryCountry(secondaryCountry)
                .date(getDate(serverRequest))
                .build();
    }

    private String getPrimaryCountry(final ServerRequest serverRequest) {
        return serverRequest.queryParam(PRIMARY_COUNTRY_ATTRIBUTE)
                .filter(supportedCountries::contains)
                .orElseThrow(() -> new BadRequestException("Primary country is missing or not supported"));
    }

    private String getSecondaryCountry(final ServerRequest serverRequest) {
        return serverRequest.queryParam(SECONDARY_COUNTRY_ATTRIBUTE)
                .filter(supportedCountries::contains)
                .orElseThrow(() -> new BadRequestException("Secondary country is missing or not supported"));
    }

    private LocalDate getDate(final ServerRequest serverRequest) {
        return serverRequest.queryParam(DATE_ATTRIBUTE)
                .map(this::parseDate)
                .orElseThrow(() -> new BadRequestException("Date is missing"));
    }

    private LocalDate parseDate(final String dateText) {
        try {
            final LocalDate date = LocalDate.from(ISO_DATE.parse(dateText));
            if (date.isAfter(LocalDate.now())) {
                throw new BadRequestException("Date mustn't be in future");
            }
            return date;
        } catch (final DateTimeParseException e) {
            throw new BadRequestException("Date in wrong format (correct: yyyy-MM-dd");
        }
    }
}
