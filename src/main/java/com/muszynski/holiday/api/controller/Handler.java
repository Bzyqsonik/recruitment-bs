package com.muszynski.holiday.api.controller;


import com.muszynski.holiday.api.exception.BadRequestException;
import com.muszynski.holiday.api.exception.CommonHolidaysNotFoundException;
import com.muszynski.holiday.api.service.HolidayService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Component
@AllArgsConstructor
public class Handler {

    private final RequestMapper requestMapper;
    private final HolidayService holidayService;

    Mono<ServerResponse> findHolidays(final ServerRequest serverRequest) {
        return requestMapper.toHolidayRequest(serverRequest)
                .map(holidayService::findHolidaysAtTheSameDayInBothCountries)
                .flatMap(it -> ServerResponse.ok().syncBody(it))
                .onErrorResume(this::getResolvedErrorResponse);
    }

    private Mono<ServerResponse> getResolvedErrorResponse(final Throwable exception) {
        return ServerResponse
                .status(getErrorStatusByException(exception))
                .syncBody(Error.of(exception.getMessage()));
    }

    private HttpStatus getErrorStatusByException(final Throwable exception) {
        if (exception instanceof BadRequestException) {
            return BAD_REQUEST;
        } else if (exception instanceof CommonHolidaysNotFoundException) {
            return NOT_FOUND;
        }
        return INTERNAL_SERVER_ERROR;
    }
}
