package com.muszynski.holiday.api.model;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.time.LocalDate;

@Value
@Builder
public class HolidayResponse {
    @NonNull
    LocalDate date;
    @NonNull
    String primaryCountryHolidayName;
    @NonNull
    String secondaryCountryHolidayName;
}
