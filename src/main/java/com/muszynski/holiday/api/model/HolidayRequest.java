package com.muszynski.holiday.api.model;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.time.LocalDate;

@Value
@Builder
public class HolidayRequest {
    @NonNull
    String primaryCountry;
    @NonNull
    String secondaryCountry;
    @NonNull
    LocalDate date;
}
