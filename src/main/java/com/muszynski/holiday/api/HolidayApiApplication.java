package com.muszynski.holiday.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class HolidayApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(HolidayApiApplication.class, args);
    }

}
