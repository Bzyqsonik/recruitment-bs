package com.muszynski.holiday.api.client.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.util.Set;

@Value
@Builder
@JsonDeserialize(builder = HolidayExternalApiResponse.HolidayExternalApiResponseBuilder.class)
public class HolidayExternalApiResponse {

    @NonNull
    Integer status;
    @NonNull
    Set<Holiday> holidays;

    @JsonPOJOBuilder(withPrefix = "")
    public static class HolidayExternalApiResponseBuilder {}
}
