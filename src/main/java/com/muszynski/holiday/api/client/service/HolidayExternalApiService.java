package com.muszynski.holiday.api.client.service;

import com.muszynski.holiday.api.client.model.Holiday;
import com.muszynski.holiday.api.client.model.HolidayExternalApiRequest;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Set;

@Service
public class HolidayExternalApiService {

    private final String holidayApiKey;
    private final HolidayExternalApiClient client;

    public HolidayExternalApiService(@Value("${holiday.api.key}") final String holidayApiKey,
                                     final HolidayExternalApiClient client) {
        this.holidayApiKey = holidayApiKey;
        this.client = client;
    }

    public Set<Holiday> findHolidays(@NonNull final HolidayExternalApiRequest request) {
        final LocalDate date = request.getDate();
        return client.findPreviousHolidays(
                holidayApiKey,
                request.getCountry(),
                getYearString(date),
                getMonthString(date),
                getDayString(date),
                true,
                true
        ).getHolidays();
    }

    private String getYearString(final LocalDate date) {
        return Integer.toString(date.getYear());
    }

    private String getMonthString(final LocalDate date) {
        final String month = Integer.toString(date.getMonthValue());
        if (month.length() == 1) {
            return "0" + month;
        } else {
            return month;
        }
    }

    private String getDayString(final LocalDate date) {
        final String day = Integer.toString(date.getDayOfMonth());
        if (day.length() == 1) {
            return "0" + day;
        } else {
            return day;
        }
    }

}
