package com.muszynski.holiday.api.client.service;

import com.muszynski.holiday.api.client.model.HolidayExternalApiResponse;
import feign.hystrix.FallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(
        name = "externalHolidayApi",
        url = "${holiday.api.url}",
        fallbackFactory = HolidayExternalApiClientFallbackFactory.class)
public interface HolidayExternalApiClient {

    String API_ENDPOINT = "/v1/holidays";
    String API_KEY_PARAM = "key";
    String PREVIOUS_HOLIDAYS_PARAM = "previous";
    String PUBLIC_HOLIDAYS_PARAM = "public";
    String COUNTRY_PARAM = "country";
    String YEAR_PARAM = "year";
    String MONTH_PARAM = "month";
    String DAY_PARAM = "day";

    @GetMapping(path = API_ENDPOINT)
    HolidayExternalApiResponse findPreviousHolidays(
            @RequestParam(API_KEY_PARAM) String key,
            @RequestParam(COUNTRY_PARAM) String country,
            @RequestParam(YEAR_PARAM) String year,
            @RequestParam(MONTH_PARAM) String month,
            @RequestParam(DAY_PARAM) String day,
            @RequestParam(PREVIOUS_HOLIDAYS_PARAM) boolean previous,
            @RequestParam(PUBLIC_HOLIDAYS_PARAM) boolean publicOnly
    );

}

class HolidayExternalApiClientFallbackFactory implements FallbackFactory<HolidayExternalApiClient> {

    @Override
    public HolidayExternalApiClient create(final Throwable cause) {
        return (key, country, year, month, day, previous, publicOnly) -> {
            throw new RuntimeException(cause);
        };
    }
}
