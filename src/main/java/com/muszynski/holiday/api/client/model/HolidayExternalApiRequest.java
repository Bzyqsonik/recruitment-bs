package com.muszynski.holiday.api.client.model;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.time.LocalDate;

@Value
@Builder
public class HolidayExternalApiRequest {
    @NonNull
    LocalDate date;
    @NonNull
    String country;
}
