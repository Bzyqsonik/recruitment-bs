package com.muszynski.holiday.api.client.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.time.LocalDate;

import static java.time.LocalDate.from;
import static java.time.format.DateTimeFormatter.ISO_DATE;

@Builder
@Value
@JsonDeserialize(builder = Holiday.HolidayBuilder.class)
public class Holiday {

    @NonNull
    String name;
    @NonNull
    String date;

    @JsonPOJOBuilder(withPrefix = "")
    public static class HolidayBuilder {
    }

    public LocalDate getFormattedDate() {
        return from(ISO_DATE.parse(date));
    }
}
