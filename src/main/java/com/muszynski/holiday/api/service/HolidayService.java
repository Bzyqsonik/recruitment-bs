package com.muszynski.holiday.api.service;

import com.muszynski.holiday.api.client.model.Holiday;
import com.muszynski.holiday.api.client.model.HolidayExternalApiRequest;
import com.muszynski.holiday.api.client.service.HolidayExternalApiService;
import com.muszynski.holiday.api.exception.CommonHolidaysNotFoundException;
import com.muszynski.holiday.api.exception.InconsistentDataException;
import com.muszynski.holiday.api.model.HolidayRequest;
import com.muszynski.holiday.api.model.HolidayResponse;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.control.Option;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static java.time.LocalDate.from;
import static java.time.LocalDate.now;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toSet;

@Service
public class HolidayService {

    private static final int MONTHS_IN_YEAR = 12;
    private final HolidayExternalApiService holidayExternalApiService;
    private final Integer monthInterval;

    public HolidayService(final HolidayExternalApiService holidayExternalApiService,
                          @Value("${holiday.api.monthInterval}") final Integer monthInterval) {
        this.holidayExternalApiService = holidayExternalApiService;
        this.monthInterval = monthInterval;
    }

    public HolidayResponse findHolidaysAtTheSameDayInBothCountries(@NonNull final HolidayRequest holidayRequest) {
        final Tuple2<LocalDate, List<HolidayWithCountry>> firstCommonHoliday = getFirstCommonHolidayUpToOneYearOrToday(holidayRequest);
        final LocalDate date = firstCommonHoliday._1;
        final String primaryCountryHolidayName = firstCommonHoliday._2
                .find(it -> holidayRequest.getPrimaryCountry().equals(it.country))
                .map(it -> it.holiday.getName())
                .getOrElseThrow(InconsistentDataException::new);
        final String secondaryCountryHolidayName = firstCommonHoliday._2
                .find(it -> holidayRequest.getSecondaryCountry().equals(it.country))
                .map(it -> it.holiday.getName())
                .getOrElseThrow(InconsistentDataException::new);
        return HolidayResponse.builder()
                .date(date)
                .primaryCountryHolidayName(primaryCountryHolidayName)
                .secondaryCountryHolidayName(secondaryCountryHolidayName)
                .build();
    }

    private Tuple2<LocalDate, List<HolidayWithCountry>> getFirstCommonHolidayUpToOneYearOrToday(final HolidayRequest holidayRequest) {
        LocalDate bottomDate = holidayRequest.getDate();
        LocalDate topDate = getBottomDatePlusThreeMonthsCappedByToday(bottomDate);
        final String primaryCountry = holidayRequest.getPrimaryCountry();
        final String secondaryCountry = holidayRequest.getSecondaryCountry();

        for (int i = 0; i < MONTHS_IN_YEAR / monthInterval; i++) {
            final Option<Tuple2<LocalDate, List<HolidayWithCountry>>> commonHolidayInInterval =
                    getFirstCommonHolidayInInterval(bottomDate, topDate, primaryCountry, secondaryCountry);
            if (commonHolidayInInterval.isDefined()) {
                return commonHolidayInInterval.get();
            } else if (topDate.isEqual(now())) {
                break;
            } else {
                bottomDate = from(topDate);
                topDate = getBottomDatePlusThreeMonthsCappedByToday(bottomDate);
            }
        }
        throw new CommonHolidaysNotFoundException("Common holiday for both countries in selected date was not found!");
    }

    private Option<Tuple2<LocalDate, List<HolidayWithCountry>>> getFirstCommonHolidayInInterval(
            final LocalDate bottomDate,
            final LocalDate topDate,
            final String primaryCountry,
            final String secondaryCountry
    ) {
        return List
                .ofAll(getHolidaysBetweenBottomDateAndThreeMonthsLater(bottomDate, topDate, primaryCountry))
                .appendAll(getHolidaysBetweenBottomDateAndThreeMonthsLater(bottomDate, topDate, secondaryCountry))
                .groupBy(it -> it.holiday.getFormattedDate())
                .filter(it -> it._2.size() == 2)
                .minBy(Tuple2::_1);
    }

    private Set<HolidayWithCountry> getHolidaysBetweenBottomDateAndThreeMonthsLater(
            final LocalDate bottomDate,
            final LocalDate topDate,
            final String country
    ) {
        LocalDate currentTopDate = from(topDate);
        final Set<Holiday> holidays = new HashSet<>();
        boolean shouldContinue = true;
        while (shouldContinue) {
            final Set<Holiday> newHolidays = getResponseForCountry(currentTopDate, country);
            final boolean allHolidaysAreAfterBottomDate = newHolidays.stream()
                    .allMatch(it -> it.getFormattedDate().isAfter(bottomDate));
            if (allHolidaysAreAfterBottomDate) {
                currentTopDate = getLowestDateOfHolidays(newHolidays);
                holidays.addAll(newHolidays);
            } else {
                holidays.addAll(newHolidays.stream()
                        .filter(it -> it.getFormattedDate().isAfter(bottomDate))
                        .collect(toSet()));
                shouldContinue = false;
            }
        }
        return holidays.stream()
                .map(it -> HolidayWithCountry.of(country, it))
                .collect(toSet());
    }

    private LocalDate getLowestDateOfHolidays(final Set<Holiday> holidays) {
        return holidays.stream()
                .min(comparing(Holiday::getFormattedDate))
                .map(Holiday::getFormattedDate)
                .orElseThrow(InconsistentDataException::new);
    }

    private Set<Holiday> getResponseForCountry(final LocalDate date, final String country) {
        return holidayExternalApiService.findHolidays(HolidayExternalApiRequest.builder()
                .country(country)
                .date(date)
                .build());
    }

    private LocalDate getBottomDatePlusThreeMonthsCappedByToday(final LocalDate bottomDate) {
        final LocalDate bottomDatePlusTwoMonths = bottomDate.plusMonths(3);
        return bottomDatePlusTwoMonths.isBefore(now()) ? bottomDatePlusTwoMonths : now();
    }

    @lombok.Value(staticConstructor = "of")
    private static class HolidayWithCountry {
        String country;
        Holiday holiday;
    }
}


